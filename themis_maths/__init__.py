"""
_DESCRIPTION
"""
__author__ = "Geremy Panthou"
__license__ = "GNU GPL"


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import time

from themis_maths.exprs import *
from themis_maths.growth import *

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
    #+++++++++++++++++++++++++++++++#
    #    SPEED TEST                 #
    #+++++++++++++++++++++++++++++++#
    
    n = 10000
    x1 = numpy.arange(n)
    x2 = numpy.random.randint(100, size=n)
    x3 = numpy.random.random(size=n)
    
    dfs = [pandas.DataFrame({"x":x*i}) for i in range(100) for x in (x1, x2, x3)]
    
    t0 = time.time()
    
    le = LinearExpr([2, 1])
    ys1 = [le(**df) for df in dfs]
    
    t1 = time.time()

    me = MathExpr('2 * x + 1')
    ys2 = [me(df) for df in dfs]
    
    
    t2 = time.time()

    print("lineexpr={0}".format(t1-t0))
    print("mathexpr={0}".format(t2-t1))
    
    assert all([(ys1[i] == ys2[i]).all() for i in range(len(dfs))])
