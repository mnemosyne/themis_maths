"""
Basic math expressions for using with numexpr.evaluate or pandas.DataFrame.eval
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
from collections.abc import Iterable
from numbers import Number
import string

import numpy
import numexpr
import pandas


from matplotlib import pyplot



##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

VARIATE = "x"
INTERCEPT = 'intercept'

##======================================================================================================================##
##                MATHExpr CLASS                                                                                        ##
##======================================================================================================================##


class MathExpr:
    """ Class doc 
    >>> m = MathExpr("X + 5")
    >>> sorted(m.__dict__.keys())
    ['expr', 'name']
    >>> m
    f = X + 5
    >>> str(m)
    'X + 5'
    >>> print(m)
    X + 5
    """
    def __init__(self, expr, name="f"):
        """ Class initialiser """
        assert isinstance(expr, str)
        assert isinstance(name, str)
        self.name = name
        self.expr = expr
        
    def __repr__(self):
        """Reprsentation of object"""
        res = f"{self.name} = {self.expr}"
        return res
    
    def __str__(self):
        """when using str or print"""
        res = self.expr
        return res
        

    def __call__(self, df):
        """Return the values for the given dataframe"""
        assert isinstance(df, pandas.DataFrame), "dataframe expected"
        res = df.eval(self)
        return res

    def plot(self, df, xkey=None, **kwarg):
        """Plot if their is only one variable """
        if len(df.columns) == 1:
            x = numpy.array(df, dtype=float)
        else:
            if xkey is None:
                x = df.index
            else:
                x = df[xkey]

        y = self(df)
        res = pyplot.plot(x, y, **kwarg)[0]
        return res

    def replace_variable(self, other):
        """
        >>> mp = MathExpr("f + g", name = 'fg')
        >>> mf = MathExpr("2 * x + b", name = 'f')
        >>> mg = MathExpr("2 * y + c", name = 'g')
        >>> mp1 = mp.replace_variable(mf)
        >>> mp2 = mp1.replace_variable(mg)
        >>> mp1
        fg = 2 * x + b + g
        >>> mp2
        fg = 2 * x + b + 2 * y + c
        """
        assert hasattr(other, "expr"), "expected exp"
        torep = other.name
        splits = self.expr.split(" ")
        news = []
        for isplt in splits:
            if isplt == torep:
                toadd = other.expr
            else:
                toadd = isplt
            news.append(toadd)
        
        newexpr = " ".join(news)
        res = MathExpr(newexpr, name=self.name)
        return res

class Polynom(MathExpr):
    """
    Cree un polynome a partir des coeffs donne
    >>> Polynom([0,1])
    Traceback (most recent call last):
            ...
    AssertionError: le nb de coeff doit etre superieur a 1 : got 2
    >>> Polynom(numpy.arange(3)).__class__ == Polynom
    True
    >>> Polynom(numpy.arange(4)).__class__ == Polynom
    True
    """
    

    def __init__(self, coefficients, name='f'):
        """
        >>> sorted(Polynom(numpy.arange(4)).__dict__.keys())
        ['coefs', 'degree', 'expr', 'name']
        """
        assert isinstance(coefficients, Iterable), f'pb entrree liste demande got {coefficients.__class__} : {coefficients}'
        assert all(isinstance(ii, Number) for ii in coefficients), 'tous les element doivent etre des nombres'
        assert len(coefficients) > 2, f'le nb de coeff doit etre superieur a 1 : got {len(coefficients)}'

        self.coefs = coefficients
        self.degree = len(coefficients) - 1
        expr = self._return_expr()
        #~ print(expr)
        MathExpr.__init__(self, expr=expr, name=name)

    def __call__(self, x):
        """
        Apply the polynom to x
        """
        res = numexpr.evaluate(str(self))
        return res
        
    def _return_expr(self):
        """
        Return polynom from given coef
        """
        p0 = str(self.coefs[0])
        res = [p0]
        for idegree, coef in enumerate(self.coefs[1:]):
            expo = idegree + 1
            pi = f"{coef}*{VARIATE}**{expo}"
            res.append(pi)
        
        res = " + ".join(res)

        return res

    def delta(self):
        """
        calcul le delta pour un polynom du second degre
        """

        assert self.degree == 2, 'delta se calcule pour un polynone de degre 2'

        c, b, a = self.coefs

        res = b ** 2 - 4 * a * c

        return res

    def solutions(self):
        """
        calcul les solution pour un polynom du second degre
        >>> p = Polynom([1,1,-2])
        >>> p.solutions() == {'x2': 1, 'x1': -1/2}
        True
        """
        delta = self.delta()
        b, a = self.coefs[1:]
        if delta < 0:
            res = 'pas de solutions dans R'
        elif delta == 0:
            res = - b / (2*a)
        else:
            x1 = (- b + delta**0.5) / (2*a)
            x2 = (- b - delta**0.5) / (2*a)

            res = {'x1' : x1, 'x2' : x2}
        return res

    def peak(self):
        """
        Renvoie le max ou le min du polynom : df/dx = 0

        >>> p= Polynom([10,-1.,-2.])
        >>> pk=p.peak()
        >>> pk["x"]
        -0.25
        >>> pk["y"]
        10.125

        >>> p= Polynom([0,1.,-2.])
        >>> p.peak() == {'y': 0.125, 'x': 0.25}
        True
        >>> p= Polynom([0,0.,-2.])
        >>> p.peak() == {'y': 0.0, 'x': 0.0}
        True
        """
        assert self.degree == 2, 'not implented for other'
        
        b, a = self.coefs[1:]
        x = - b / (2. * a)
        y = float(self(x))
        
        res = {"x" : x, "y" : y}
        
        return res
        
    def plot(self, x, **kwarg):
        """
        Plot the polynom
        """
        x = numpy.array(x, dtype=float)
        y = self(x)
        res = pyplot.plot(x, y, **kwarg)[0]
        return res
            
    def plot2(self, x, **kwarg):
        """
        >>> f = pyplot.figure()
        >>> p= Polynom([0,10.,-2.])
        >>> d = p.plot2(x = numpy.arange(-5,5))
        >>> f.show()
        """
        l = self.plot(x, **kwarg)
        
        xsol = self.solutions()
        
        if isinstance(xsol, Number):
            l1 = pyplot.plot(xsol, 0, marker="s", color="k")
        elif isinstance(xsol, dict):
            xs = list(xsol.values())
            l1 = pyplot.plot(xs, [0] * 2, marker="s", color="k", ls="")
        else:
            l1 = None
        
        xymax = self.peak()
        x = xymax["x"]
        y = xymax["y"]
        l2 = pyplot.plot(x, y, marker="o", color="k")
                
        res = {"l"     :       l, "l1"    :       l1, "l2"    :       l2}
        
        return res





class LinearCombination(MathExpr):
    """
    Renvoie un relation lineaire entre covariable
    >>> LinearCombination('x').expr
    '(a*x)+b'
    >>> LinearCombination(('x', 'y')).expr
    '(a*x)+(b*y)+c'
    >>> LinearCombination({'intercept': 'u0', 'lat' :'u1', 't' :'u2'}).expr
    '(u1*lat)+(u2*t)+u0'
    >>> l = LinearCombination({'lat':2, 'intercept':1})
    >>> l.expr
    '(2*lat)+1'
    >>> lneg = LinearCombination({'lat':-2, 'intercept':1})
    >>> lneg.expr
    '-(2*lat)+1'
    >>> df = pandas.DataFrame({'lat' : numpy.arange(10, dtype = int)})
    >>> print(l(df).to_string())
    0     1
    1     3
    2     5
    3     7
    4     9
    5    11
    6    13
    7    15
    8    17
    9    19
    >>> f = pyplot.figure()
    >>> line=l.plot(df)
    >>> f.show()
    
    >>> lneg = LinearCombination({'lat':-2.6, 'intercept':-1.05})
    >>> lneg.expr
    '-(2.6*lat)-1.05'
    >>> df = pandas.DataFrame({'lat' : numpy.arange(10, dtype = int)})
    >>> print(lneg(df).to_string())
    0    -1.05
    1    -3.65
    2    -6.25
    3    -8.85
    4   -11.45
    5   -14.05
    6   -16.65
    7   -19.25
    8   -21.85
    9   -24.45
    >>> f = pyplot.figure()
    >>> line=l.plot(df)
    >>> f.show()
    
    
    
    >>> l1 = LinearCombination({'lat':2, 'intercept':1})
    >>> l2 = LinearCombination({'lat':2, 'intercept':2})
    >>> l3 = LinearCombination({'lat':2, 'intercept':1, "lon":5})
    
    >>> l.params()
    intercept    1
    lat          2
    dtype: int64
    >>> l1.params()
    intercept    1
    lat          2
    dtype: int64

    >>> l2.params()
    intercept    2
    lat          2
    dtype: int64
    >>> l3.params()
    intercept    1
    lat          2
    lon          5
    dtype: int64
    
    
    >>> l == l1
    True
    >>> l == l2
    False
    >>> l == l3
    False
    
    """
    
    def __init__(self, dic_covpar, name='f'):
        """
        >>> sorted(LinearCombination(('x', 'y')).__dict__.keys())
        ['dic_covpar', 'expr', 'name']
        """
        if isinstance(dic_covpar, dict):
            pass
        else:
            if isinstance(dic_covpar, Iterable):
                assert all(isinstance(i, str) for i in dic_covpar), 'expected "str" for all cov'
                covariates = list(dic_covpar)
            else:
                assert isinstance(dic_covpar, str), f"pb input : {dic_covpar}"
                covariates = [dic_covpar]

            covariates += [INTERCEPT]
            parameters = [string.ascii_lowercase[icase] for icase in range(len(covariates))]
            dic_covpar = {}
            for k, v in zip(covariates, parameters):
                dic_covpar[k] = v

        #predicteurs: regressors, endogenous variables, explanatory variables, covariates, 
        #input variables, predictor variables,or independent variables
        self.dic_covpar = dic_covpar

        #predictant: regressand exogenous variable, response variable, measured variable, or dependent variable
        #~ self.pred_var = pred_var

        expr = self._return_expr()
        MathExpr.__init__(self, expr=expr, name=name)


    def _return_expr(self):
        """
        Renvoie l'expression a partir du nom des covariables
        """
        res = ""

        #~ varkeys = sorted(self.dic_covpar.keys())
        dic = self.dic_covpar.copy()
        
        if INTERCEPT in dic:
            intercept = dic.pop(INTERCEPT)
            intercept = "{0}".format(intercept)
            
        else:
            intercept = None
            #~ varkeys = [i for i in varkeys if i != "intercept"] + ["intercept"]
            #~ assert all([i in self.dic_covpar for i in varkeys]), "pbcode"
        
        res = []
        for var, par in sorted(dic.items()):
            assert isinstance(var, str), f'pb covar must be str, intercept for nocov. Got {var}, {var.__class__}'
            p = str(par)
            toadd = f"({p}*{var})"
            toadd = toadd.replace("(-", "-(") #for pb in pandas.eval
            res.append(toadd)
        
        if intercept is not None:
            res.append(intercept)
            
        res = "+".join(res)
        res = res.replace('+-', '-')#for pb in pandas.eval
        res = res.strip()
        return res

    def varkeys(self):
        """
        Return the variate keys
        """
        res = [i for i in self.dic_covpar.keys() if i != INTERCEPT]
        if len(res) == 1:
            res = res[0]
        return res

    def params(self):
        """Return a series containeing the paramters"""
        res = pandas.Series(self.dic_covpar, index=sorted(self.dic_covpar))
        return res
        

    def __eq__(self, other):
        """Test wether the other is equal"""
        if isinstance(other, LinearCombination):
            p1 = self.params()
            p2 = other.params()
            if p1.index.size == p2.index.size and (p1.index.sort_values() == p2.index.sort_values()).all():
                res = (p1 == p2).all()
            else:
                res = False
        else:
            res = False
        
        return res

##======================================================================================================================##
##                MATHExpr CLASS                                                                                        ##
##======================================================================================================================##


class Expr:
    """ Test input args
    >>> e=Expr([0,2,3])
    >>> e.a
    0.0
    >>> e.b
    2.0
    >>> e.c
    3.0
    >>> e=Expr(*[2,3,4])
    >>> e.a
    2.0
    >>> e.b
    3.0
    >>> e.c
    4.0
    >>> e=Expr(3,2,1)
    >>> e.a
    3.0
    >>> e.b
    2.0
    >>> e.c
    1.0
    >>> e=e.new([5])
    >>> e.a
    5.0
    >>> e.b
    Traceback (most recent call last):
        ...
    AttributeError: 'Expr' object has no attribute 'b'
    >>> e=e.new([4,6,8])
    >>> e.a
    4.0
    >>> e=e.new(3.5,9,8,9,6)
    >>> e.a
    3.5
    >>> e.b
    9.0
    >>> e.c
    8.0
    >>> e.d
    9.0
    >>> e.e
    6.0
    >>> e.f
    Traceback (most recent call last):
        ...
    AttributeError: 'Expr' object has no attribute 'f'
    """
    def __init__(self, *args):
        """ Class initialiser """
        if len(args) == 1 and isinstance(args[0], Iterable):
            args = args[0]
        # ~ print(args)
        npar = len(args)
        parnames = string.ascii_lowercase[:npar]
        for kpar, vpar in zip(parnames, args):
            setattr(self, kpar, float(vpar))
        
    def __call__(self, x, **kwargs):
        """Apply the function"""
        res = None
        return res
        
    def plot(self, xlim, **kwargs):
        """Plot"""
        x = numpy.linspace(*xlim, num=1000)
        y = self(x)
        res = pyplot.plot(x, y, **kwargs)[0]
        return res
        
    def new(self, *args):
        """New expr having the same formulation"""
        res = self.__class__(*args)
        return res
        
    def npar(self):
        """See doctest of inherits classes"""
        res = len(self.__dict__)
        return res
        
    def parnames(self):
        """See doctest of inherits classes"""
        res = sorted(self.__dict__)
        return res
        
    def params(self):
        """See doctest of inherits classes"""
        res = pandas.Series(self.__dict__, index=self.parnames())
        return res


# ~ class ConstantExpr(Expr):
    # ~ """ Class doc
    # ~ >>> 'afarie' """
    
    # ~ def __init__(self, *args):
        # ~ """ Class initialiser """
        # ~ Expr.__init__(self, *args)

    # ~ def __call__(self, x, **kwargs):
        # ~ """Apply the fuction"""
        # ~ res = x * 0 + self.a
        # ~ return res
        
    # ~ def new(self, *args):
        # ~ """New with same params"""
        # ~ res = self.__class__(*args)
        # ~ return res
        

class InterceptExpr(Expr):
    """ Class doc
    >>> le = InterceptExpr(1)
    >>> fig = pyplot.figure()
    >>> l=le.plot((0,10))
    >>> fig.show()
    >>> fig = pyplot.figure()
    >>> l=le.plot((0,100), marker='o', ls='--', color='r')
    >>> fig.show()
    
    >>> le.npar()
    1
    >>> le.params()
    a    1.0
    dtype: float64
    >>> le.parnames()
    ['a']
    """
    def __init__(self, *args):
        """ Class initialiser """
        Expr.__init__(self, *args)
        
    def __call__(self, x, **kwargs):
        """Apply the fuction"""
        res = x * 0 + self.a
        return res

class LinearExpr(Expr):
    """ Class doc
    >>> le = LinearExpr(-1,1)
    >>> fig = pyplot.figure()
    >>> l=le.plot((0,10))
    >>> fig.show()
    >>> fig = pyplot.figure()
    >>> l=le.plot((0,100), marker='o', ls='--', color='r')
    >>> fig.show()
    >>> le.npar()
    2
    >>> le.params()
    a   -1.0
    b    1.0
    dtype: float64
    >>> le.parnames()
    ['a', 'b']
    """
    def __init__(self, *args):
        """ Class initialiser """
        Expr.__init__(self, *args)
        
    def __call__(self, x, **kwargs):
        """Apply the fuction"""
        res = self.a * x + self.b
        return res
        




##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='Samle module')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        from japet_misc import close_all, show_all
        close_all()
        d_f = pandas.DataFrame(numpy.arange(25).reshape(5, 5), columns=list("abcde"))
        lc = LinearCombination({"a" : 2, "b" : 3, "e": 1})
        print(lc)
        print(d_f)
        print(lc(d_f))
        pyplot.figure()
        mp = MathExpr("10 * a ** b")
        print(mp(d_f))
        mp = MathExpr("10 ** (b / 10)")
        mp.plot(d_f)
        show_all()
        
        
        xe = numpy.arange(100)
        
        le = LinearExpr(1, 5)
        ye = le(xe)
        
        ie = InterceptExpr(1, 5)
        ye2 = ie(xe)
        
        
