"""
_DESCRIPTION
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import math
from numbers import Number
import numpy
import pandas

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

class Growth:
    """ Class doc
    >>> g=Growth(1, 0.03)
    >>> round(g.characteristic_time(), 0)
    Doubling time
    23.0
    >>> round(g.value(10), 1)
    1.3
    >>> round(g.value(25), 1)
    2.1
    >>> round(g.value(50), 1)
    4.4
    >>> round(g.value(100), 1)
    19.2
    
    """
    
    def __init__(self, init_value, percentage):
        """ Class initialiser """
        assert isinstance(init_value, Number)
        assert isinstance(percentage, Number)
        self.init_value = init_value
        self.percentage = percentage
        self.increment = 1 + self.percentage
        
        
    def value(self, n_iter):
        """Return the value at a given iteration
        """
        res = self.init_value * self.increment **n_iter
        return res
        
        
    def to_ts(self, max_iter):
        """To Timeseires"""
        inds = numpy.arange(max_iter+1)
        vals = [self.value(i) for i in inds]
        res = pandas.Series(vals, index=inds)
        
        return res
        
    def characteristic_time(self):
        """Give the doubling time or the Halflife time"""
        if self.percentage > 0:
            print("Doubling time")
            res = math.log(2)/math.log(self.increment)
        else:
            print("Halflife time")
            res = -math.log(2)/math.log(self.increment)
            
        
        return res

##======================================================================================================================##
##                CLASSES                                                                                               ##
##======================================================================================================================##



##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
